# joshvak
A split DVORAK layout with thumb modifiers for the idobo XD75re keyboard.

Download the file "joshvak.json" then head over to https://config.qmk.fm/# to upload and compile.

Full instructions & downloads for QMK are available at https://qmk.fm/
